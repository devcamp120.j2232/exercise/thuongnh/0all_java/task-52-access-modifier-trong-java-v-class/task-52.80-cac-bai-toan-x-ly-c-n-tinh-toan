public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("khu vực của task1 : >>>>>>>>>>>>>>>>>>>>" );
        LopKhac.task1(2.100212, 2);
        LopKhac.task1(2100, 3);
        // task2 
        System.out.println("khu vực của task2 : >>>>>>>>>>>>>>>>>>>>" );
        System.out.println("random từ 0 đến 1 " + LopKhac.task2(0, 1));
        System.out.println( "random từ 1 đến 20 " + LopKhac.task2(1, 20));
        System.out.println("random từ 10 đến 20 " + LopKhac.task2(10, 20));
        //task 3
        System.out.println("khu vực của task3 : >>>>>>>>>>>>>>>>>>>>" );
        System.out.println(  "đầu vào là 2 và 4:   " +  LopKhac.task3(2,4));
        System.out.println("đầu vào là 3 và 4:   " + LopKhac.task3(3,4));

        System.out.println("khu vực của task4 kiểm tra số chính phương  : >>>>>>>>>>>>>>>>>>>>" );
        System.out.println( "đầu vào là 64:   " + LopKhac.task4(64) );
        System.out.println("đầu vào là 65:   " + LopKhac.task4(65) );
        System.out.println("đầu vào là 81:   " + LopKhac.task4(81) );

        System.out.println("khu vực của task5 Lấy số lớn hơn gần nhất của số đã cho chia hết cho 5   : >>>>>>>>>>>>>>>>>>>>" );
        System.out.println("đầu vào là 132:  đầu ra là: " + LopKhac.task5(132));
        System.out.println("đầu vào là 137:  đầu ra là: " + LopKhac.task5(137));
        
        System.out.println("khu vực của task6 : Kiểm tra đầu vào có phải số hay không >>>>>>>>>>>>>>>>>>>>" );
        System.out.println("12 có phải là số hay không: " + LopKhac.task6( "12") );
        System.out.println("abcd có phải là số hay không: " + LopKhac.task6( "abcd") );
        System.out.println("12.6 có phải là số hay không: " + LopKhac.task6( "12.5") );

        System.out.println("khu vực của task7 : Kiểm tra số đã cho có phải lũy thừa của 2 hay không   >>>>>>>>>>>>>>>>>>>>" );
        System.out.println("16 có phải là lũy thừa của 2 : " +  LopKhac.task7(16));
        System.out.println("18 có phải là lũy thừa của 2 : " +  LopKhac.task7(18));
        System.out.println("256 có phải là lũy thừa của 2 : " +  LopKhac.task7(256));

        System.out.println("khu vực của task8 : Kiểm tra số đã cho có phải số tự nhiên hay không  >>>>>>>>>>>>>>>>>>>>" );
        System.out.println("-15 có phải là số tự nhiên hay không  : " +  LopKhac.task8(-15));
        System.out.println("1 có phải là số tự nhiên hay không  : " +  LopKhac.task8(1));
        System.out.println("1.2 có phải là số tự nhiên hay không  : " +  LopKhac.task8(1.2));

        System.out.println("khu vực của task9 : Thêm dấu , vào phần nghìn của mỗi số  >>>>>>>>>>>>>>>>>>>>" );
        System.out.println("đầu vào là  1000   đầu ra là : " +  LopKhac.task9(1000));
        System.out.println("đầu vào là  10000.23   đầu ra là : " +  LopKhac.task9(10000.23));
        System.out.println("đầu vào là  123456789   đầu ra là : " +  LopKhac.task9(1234567));

        System.out.println("khu vực của task10 : >>>>>>>>>>>>>>>>>>>>" );
        System.out.println(LopKhac.task10(51));
        System.out.println(LopKhac.task10(4));

    }

}

// một lớp khác

class LopKhac {
    // task 1 Làm tròn n số sau dấu .
    public static double task1(double paramSo, int paramN) {
        // 1 Nhân số thực cần làm tròn với 10^n để dời dấu phẩy n chữ số sang phải.
        // 2 Làm tròn số nguyên thu được từ bước trên bằng cách thêm 0.5 và lấy phần
        // nguyên.
        // 3 Chia kết quả thu được từ bước trên cho 10^n để đưa dấu phẩy trở lại vị trí
        // ban đầu.
        // 4 Trả về số thực đã làm tròn.
        double factor = Math.pow(10, paramN); // tạo ra số bị chia
        int rounded = (int) (paramSo * factor + 0.5); // ép kiểu là int
        System.out.printf("%." + paramN + "f", (double) rounded / factor);
        System.out.printf("\n");
        return (double) rounded / factor;
    }

    // task 2 Lấy 1 số random bất kỳ trong khoảng 2 số cho trước
    public static int task2(int paramMin, int paramMax) {
        int range = paramMin - paramMax + 1;   // Tính toán phạm vi của khoảng bằng cách lấy hiệu của giá trị lớn nhất và giá   trị nhỏ nhất.
        double random = Math.random();  // Sử dụng phương thức Math.random để sinh ra một số ngẫu nhiên trong khoảng từ 0 đến 1.
        // Nhân số ngẫu nhiên thu được từ bước trên với phạm vi của khoảng. Cộng kết quả thu được từ bước trên với giá trị nhỏ nhất của khoảng.
        // Lấy phần nguyên của kết quả thu được từ bước trên.
        int randomInRange = (int) (random * range) + paramMin; 
        return randomInRange;
    }
    // task 3 Tính số pytago từ 2 số đã cho c2 = a2 + b2  ;c = căn của  a2 + b2 
    public static double task3(double paramA, double paramB) {
        return (double) Math.sqrt(paramA*paramA + paramB*paramB);
    }

    // task 4 Kiểm tra số đã cho có phải số chính phương hay không c = a2
    public static boolean task4(int paramA){
        double canbac2 =  Math.sqrt(paramA); // căn bậc 2 số đã cho 
        // nếu  chia hết cho 1 . thì nó là số chính phương return true .. ngược lại thì false
        if(canbac2 %1 == 0){
            return true;
        }
        else{
            return false;
        }
    }
    // task5 Lấy số lớn hơn ,gần nhất , của số đã cho chia hết cho 5
    public static int task5(int paramA){
        // dùng vòng lặp kiểm tra số trên chia hết cho 5 k.. nếu không thì tăng 1 đơn vị.. dừng vòng lặp khi đã chia hết cho 5 ;
        boolean vDieuKien = true;   // cho  vòng lặp chạy vì chưa tìm được số thích hơp
        int vResult = -1;
        while(vDieuKien){
            if(paramA % 5 == 0){
                vDieuKien =  false;
                vResult = paramA;
            }
            else {
                paramA++;
            }
        }
        return paramA;
        }
    // task 6 Kiểm tra đầu vào có phải số hay không
    //Sử dụng phương thức parse() của lớp Number: 
    //Ta có thể thử ép kiểu chuỗi thành số và xem có lỗi xảy ra hay không. Nếu không có lỗi, thì đầu vào đó là số.
    public static boolean task6(String inputString) {
        try {
            Double.parseDouble(inputString);
            return true;
        } catch (NumberFormatException e) {
            // System.out.println("mã lỗi:   " +  e);
            return false;
        }
        
    }

    // task 7 Kiểm tra số đã cho có phải lũy thừa của 2 hay không
    public static boolean task7(int number) {
        if (number <= 0) {
            return false;  //Kiểm tra number có nhỏ hơn hoặc bằng 0 không. Nếu có, trả về false vì không có số 
        }
        int logA = (int) (Math.log(number) / Math.log(2));  // tính log cơ số 2 của số đầu vào... và ép kiểu thành int 
        return Math.pow(2, logA) == number;  //   bình phương logA xem có bằng số đầu vào hay không
    }
    // task8 Kiểm tra số đã cho có phải số tự nhiên hay không
    public static boolean task8(double number) {
        // number != (int)number  : kiểm tra xem mumber có phải kiểu double hay k
        // 
        if (number <= 0 || number != (int)number) {
            return false;
        }
        return true;
    }

    // task 9 Thêm dấu , vào phần nghìn của mỗi số 
    public static String task9(double number) {
        String str = String.valueOf((int)number);  // chuyển số thành string 
        String strDouble = String.valueOf(number);  // chuyển số  double thành string 
        //  tách số thực thành 2 phần == phần nguyên và phần thập phân..
        // phần nguyên sẽ được dùng hàm gắn thêm dấu phẩy ở dưới 
        // phần thập phân sẽ được nối trực tiếp sau cùng 
        // phát hiện phần thập phân qua xử lý chuỗi 
        int decimalIndex = strDouble.indexOf(".");  // vị trí của dấu "."  trong chuỗi 
        String decimalPart = strDouble.substring(decimalIndex );  //cắt  lấy chuỗi sau dấu "."
        String formatted = "";  // khai báo chuỗi cần trả về 
        // nếu số thập phân phía sau == 0 .. thì bỏ cộng thêm  decimalPart
        boolean isKetQua = (decimalPart.equals(".0"));  // sử dụng phương thức equals thay vì == .   == để  so sánh địa chỉ bộ nhớ.. 
        // bởi vì hai đối tượng khác nhau nên chúng k cùng 1 ô nhớ
        if(!isKetQua){
            formatted = decimalPart;   // nếu số thập phân phía sau !== 0 .. thì cộng thêm  decimalPart
        }
        int count = 0;
        for (int i = str.length() - 1; i >= 0; i--) {
            count++;
            formatted = str.charAt(i) + formatted;
            if (count % 3 == 0 && i != 0) {
                formatted = "," + formatted;
            }
        }
        return formatted;
    }

    // task 10 Chuyển số từ hệ thập phân về hệ nhị phân
    public static String task10(int decimal) {
        if (decimal == 0) {
            return "0";
        }
        String binary = "";
        while (decimal > 0) {
            int bit = decimal % 2;
            binary = bit + binary;
            decimal /= 2;
        }
        return binary;
    }



}
