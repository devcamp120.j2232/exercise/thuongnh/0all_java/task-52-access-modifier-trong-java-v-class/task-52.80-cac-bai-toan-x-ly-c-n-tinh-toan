import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ScreenCapture {
    
    public static void main(String[] args) {
        try {
            // Get the default toolkit
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            // Get the screen size
            Dimension screenSize = toolkit.getScreenSize();
            // Create a rectangle representing the entire screen
            Rectangle screenRect = new Rectangle(screenSize);
            // Create a robot to capture the screen
            Robot robot = new Robot();
            // Capture the screen
            BufferedImage image = robot.createScreenCapture(screenRect);
            // Save the captured image to a file
            File file = new File("screenshot.png");
            ImageIO.write(image, "png", file);
            System.out.println("Screenshot saved to: " + file.getAbsolutePath());
        } catch (AWTException | IOException ex) {
            ex.printStackTrace();
        }
    }
}
